package polymorphism;

public class TangoDancer extends Dancer {
	public TangoDancer(String name, int age) {
		super(name, age);
	}
	@Override
	public void dance() {
		System.out.println(toString() + " I'm dance tango");
	}
}
