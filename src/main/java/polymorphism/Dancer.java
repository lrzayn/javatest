package polymorphism;

public class Dancer {
	private String name;
	private int age;
	
	public Dancer(String name, int age) {
		this.name = name;
		this.age = age;		
	}
	
	public void dance() {
		System.out.println(toString() + " I'm dance as all");
	}
	
	@Override
	public String toString() {
		return "I " + name + " I'm " + age + " years old";
	}
}
