package polymorphism;

public class WalzDancer extends Dancer{
	public WalzDancer(String name, int age) {
		super(name, age);
	}
	@Override
	public void dance() {
		System.out.println(toString() + " I'm dance walz");
	}

}
