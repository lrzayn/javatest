package polymorphism;

import java.util.Arrays;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		Dancer dancer = new Dancer("Tom", 20);
		Dancer tangoDancer = new TangoDancer("Bill", 21);
		Dancer walzDancer = new WalzDancer("Zena", 19);
		
		List<Dancer> place = Arrays.asList(dancer, tangoDancer, walzDancer);
		for (Dancer d : place) {
			d.dance(); //polymorphic call of methods from all classes
		}
	}
}
